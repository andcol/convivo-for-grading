from flask_pymongo import pymongo
from flask import Flask
from flask import Flask,jsonify,request
import sys
client = pymongo.MongoClient('<DOCUMENT_DB_URI>',connectTimeoutMS=30000, socketTimeoutMS=None, socketKeepAlive=True, connect=False, maxPoolsize=1)
users = client.UserDB.UserCollection
matches = client.MatchesDB.MatchesCollection
#client.close()

MAXAMOUNTOFUSERS = 20


app = Flask(__name__)

# GET /api/matches/<userid>
# Must be authorized and identity-verified for <userid>
#
# Returns 200 OK if successful. The response includes the top 20 matches for user <userid>, sorted by decreasing compatibility score.
#                               Matches from the database are transformed so that only the score, id and profile of the other users are left.
# Returns 404 User does not exist if <userid> is not in the user database.

@app.route('/api/matches/<userid>' , methods=['GET'])
def get_matches(userid):
    user = users.find_one({"userId" :  userid } )
    if user == None:
        return 'User does not exist', 404
    else:
        cur = matches.find({ '$query': {'$or':[{'u1':userid},{'u2':userid}]}, '$orderby': { 'score' : -1 } }).limit(MAXAMOUNTOFUSERS)
        results = []
        for matchdoc in cur:
            otheruserid = matchdoc['u1'] if matchdoc['u2'] == userid else matchdoc['u2']
            userprofile = users.find_one({'userId' : otheruserid})['profile']
            results.append({
                'userId': otheruserid,
                'score': matchdoc['score'],
                'profile': userprofile
            })
        return {'matches':results}
        
# GET /api/matches/<user1id>/<user2id>
# Must be authorized and identity-verified for <user1id>
#
# Returns 200 OK if successful. The response is a single match object describing the match between <user1id> and <user2id>
#                               Matches from the database are transformed so that only the score, id and profile of <user2id> are left.
# Returns 404 User does not exist if either <user1id> or <user2id> are not in the user database.

@app.route('/api/matches/<user1id>/<user2id>' , methods=['GET'])
def get_match(user1id, user2id):
    user1 = users.find_one({'userId' : user1id})
    user2 = users.find_one({'userId' : user2id})
    if user1 == None or user2 == None:
        return 'One or both users do not exist', 404
    else:
        match = matches.find_one({'$and':[{'$or':[{'u1':user1id},{'u2':user1id}]},{'$or':[{'u1':user2id},{'u2':user2id}]}]})
        if match == None:
            return {
                'userId': user2id,
                'score': 0,
                'profile': user2['profile']
            }
        else:
            return {
                'userId': user2id,
                'score': match['score'],
                'profile': user2['profile']
            }

if __name__== "__main__":
    app.run(host="0.0.0.0", port=88)