from flask_pymongo import pymongo
from flask import Flask
from flask import Flask,jsonify,request
import sys
import requests
client = pymongo.MongoClient('<DOCUMENT_DB_URI>',connectTimeoutMS=30000, socketTimeoutMS=None, socketKeepAlive=True, connect=False, maxPoolsize=1)
db = client.UserDB
col = db.UserCollection
#client.close()



app = Flask(__name__)

#email notifications

from mailjet_rest import Client
import os
api_key = '<MAILJET_API_KEY>'
api_secret = '<MAILJET_API_SECRET>'
mailjet = Client(auth=(api_key, api_secret), version='v3.1')

#The following functions fill out email templates with actual user data so that mailjet can send the payload
def fillWelcomeTemplate(userAddress, userName):
    return {
        'Messages': [
            {
            "From": {
                "Email": "convivowebapp@gmail.com",
                "Name": "Convivo"
            },
            "To": [
                {
                "Email": userAddress,
                "Name": "Andrea"
                }
            ],
            "Subject": "Welcome to Convivo!",
            "TextPart": "Welcome to Convivo",
            "HTMLPart": f'<h3>Dear {userName}, welcome to Convivo!</h3><p>We wish you a pleasant experience!</p>',
            "CustomID": "Welcome"
            }
        ]
    }

def fillMatchTemplate(userAddress, otherUserAddress):
    return {
        'Messages': [
            {
            "From": {
                "Email": "convivowebapp@gmail.com",
                "Name": "Convivo"
            },
            "To": [
                {
                "Email": userAddress,
                "Name": "Andrea"
                }
            ],
            "Subject": "You have a match!",
            "TextPart": "You have a match",
            "HTMLPart": f'<h3>You have recently matched with someone, congratulations!</h3><p>You can get in contact with you match by writing to {otherUserAddress}</p>',
            "CustomID": "Match"
            },
            {
            "From": {
                "Email": "convivowebapp@gmail.com",
                "Name": "Convivo"
            },
            "To": [
                {
                "Email": otherUserAddress,
                "Name": "Andrea"
                }
            ],
            "Subject": "You have a match!",
            "TextPart": "You have a match",
            "HTMLPart": f'<h3>You have recently matched with someone, congratulations!</h3><p>You can get in contact with you match by writing to {userAddress}</p>',
            "CustomID": "Match"
            }
        ]
    }

#The address of the lambda function updating compatibility scores.
COMPUTESERVICE = 'https://cnxnv80he1.execute-api.us-east-1.amazonaws.com/test/updatescores'

# GET /api/users/<userid>
# Must be authorized and identity-verified for <userid>
#
# Returns 200 OK if successful. Response consists of all user data for <userid>.
# Returns 404 User does not exist if <userid> is not in the user database.
@app.route('/api/users/<userid>' , methods=['GET'])
def get_user_test(userid):
    doc = col.find_one({"userId" :  userid } )
    if doc == None:
        return 'User does not exist', 404
    else:
        del doc['_id']
        return doc

# GET /api/users/<userid>/profile
# Must be authorized
#
# Returns 200 OK if successful. Response consists of only the profile data of <userid>.
# Returns 404 User does not exist if <userid> is not in the user database.
@app.route('/api/users/<userid>/profile' , methods=['GET'])
def get_profile_test(userid):
    doc = col.find_one({"userId" :  userid } )
    if doc == None:
        return 'User does not exist', 404
    else:
        return doc['profile']

# PUT /api/users/<userid>
# Must be authorized and identity-verified for <userid>
# Payload includes a User object
#
# Returns 200 OK if successful. User data corresponding to <userid> is created in the users database.
# Returns 403 User already exists if <userid> is already in the users database.
@app.route('/api/users/<userid>', methods=['PUT'])
def put_user_test(userid):
    if col.find_one({"userId" :  userid }) != None:
        return 'User already exists', 403
    else:
        payload = request.json
        #insert new user into DB
        newuser = {
            'userId': userid,
            'notifications': [{'userId':'','type':'welcome'}],
            'failedMatches':[],
            'profile':payload
        }
        col.insert_one(newuser)
        #send the user data to the compute service
        del newuser['_id']
        r = requests.post(COMPUTESERVICE, json=newuser, headers={
            'Content-Type':'application/json',
            'X-Amz-Invocation-Type': 'Event',
            'X-Amz-Log-Type': 'Tail'
        })
        #notify the user
        result = mailjet.send.create(data=fillWelcomeTemplate(payload['email'], payload['displayName']))
        print (result.status_code)
        print (result.json())

        return 'Done'


# PUT /api/users/<userid>/profile (not used)
# Must be authorized and identity-verified for <userid>
# Payload includes a Profile object
#
# Returns 200 OK. User profile of <userid> is created or updated in the users database.
@app.route('/api/users/<userid>/profile', methods=['PUT'])
def put_profile_test(userid):
    payload = request.json
    col.update({'userId' : userid}, {'$set':{'profile' : payload}})
    return 'Done'


# POST /api/users/<userid>/notifications
# Must be authorized
# Payload includes a Notification object
#
# Returns 200 OK. A new notification is appended to <userid>'s notifications in the users database.
@app.route('/api/users/<userid>/notifications', methods=['POST'])
def post_notification_test(userid):
    #add to the database
    payload = request.json
    col.update({'userId' : userid}, {'$push':{'notifications' : payload}})
    #notify users if notification is of accepted match
    if payload['type'] == 'accepted':
        user1 = col.find_one({'userId':userid}, {'profile.email':1})
        user2 = col.find_one({'userId':str(payload['userId'])}, {'profile.email':1})
        print(payload)
        result = mailjet.send.create(data=fillMatchTemplate(user1['profile']['email'], user2['profile']['email']))
        print (result.status_code)
        print (result.json())
    return 'Done'

#future (not used)
@app.route('/api/users/<userid>/notifications', methods=['DELETE'])
def delete_notifications_test(userid):
    return ({'message' : "Done"})

if __name__== "__main__":
    app.run(host="0.0.0.0", port=80)
