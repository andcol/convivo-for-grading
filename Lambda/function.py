import json
from pymongo import MongoClient
import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#Strong preferences
ABSYES = 2
ABSNO = -2

#Thresholds
AGEWINDOW = 3
BUDGETTOLERANCE = 0.2

MAXSCORE = 24 #actually 28, trimmed for usability
MINSCORE = -20 #actually -22, trimmed for usability


def lambda_handler(event, context):
    client = MongoClient('<DOCUMENT_DB_URI>')
    users = client.UserDB.UserCollection
    matches = client.MatchesDB.MatchesCollection
    #The new or updated user object is sent to the algorithm by the users service
    payload = json.loads(event['body'])
    compute(payload, users, matches)
    return {
        'statusCode': 200,
        'body': json.dumps('computation started')
    }


def compute(cu, userCollection, matchesCollection):
    
    # The first step is to extract candidates from the database. A candidate is a user of the same city as the current user who is NOT the current user and who
    # does not violate any of the current user's strong preferences ("Absolutely yes" or "Absolutely not") (PART 1). Conversely, the current user should not violate a
    # candidate's strong preferences (PART 2). The following query is build:
    
    currentUser=cu['profile']
    query = {'userId':{'$ne':cu['userId']}, 'profile.targetCity':currentUser['targetCity']}

    #query for the fields that are in personalData
    personalQuery = {}

    #if the current user absolutely wants to live with people of the same gender, get only people of the same gender from the database
    if currentUser['preferences']['sameGenderPref'] == ABSYES:
        personalQuery['gender'] = currentUser['personalData']['gender']
    #conversely for a negative preference
    elif currentUser['preferences']['sameGenderPref'] == ABSNO:
        personalQuery['gender'] = {'$ne': currentUser['personalData']['gender']}

    #ditto with nationality
    if currentUser['preferences']['sameNationalityPref'] == ABSYES:
        personalQuery['nationality.code'] = currentUser['personalData']['nationality']['code']
    elif currentUser['preferences']['sameNationalityPref'] == ABSNO:
        personalQuery['nationality.code'] = {'$ne': currentUser['personalData']['nationality']['code']}

    if currentUser['preferences']['sameAgePref'] == ABSYES:
        personalQuery['age'] = {'$gte': currentUser['personalData']['age']-AGEWINDOW, '$lte':currentUser['personalData']['age']+AGEWINDOW}
    elif currentUser['preferences']['sameAgePref'] == ABSNO:
        personalQuery['age'] = {'$lte': currentUser['personalData']['age']-AGEWINDOW, '$gte':currentUser['personalData']['age']+AGEWINDOW}

    #query for the fields that are in livingData
    livingQuery = {}
    
    #ditto
    if currentUser['preferences']['sameOccupationPref'] == ABSYES:
        livingQuery['occupation'] = currentUser['livingData']['occupation']
    elif currentUser['preferences']['sameOccupationPref'] == ABSNO:
        livingQuery['occupation'] = {'$ne': currentUser['livingData']['occupation']}

    if currentUser['preferences']['childrenPref'] == ABSYES:
        livingQuery['hasChildren'] = True
    elif currentUser['preferences']['childrenPref'] == ABSNO:
        livingQuery['hasChildren'] = False

    if currentUser['preferences']['petsPref'] == ABSYES:
        livingQuery['hasPets'] = True
    elif currentUser['preferences']['petsPref'] == ABSNO:
        livingQuery['hasPets'] = False

    #in this case, the values that are acceptable for a current user who absolutely needs to live with clean people are in a range (0: cleans normlly or 1: is a clean freak)
    if currentUser['preferences']['cleaningPref'] == ABSYES:
        livingQuery['cleaningHabits'] = {'$gte':0}
    elif currentUser['preferences']['cleaningPref'] == ABSNO:
        livingQuery['cleaningHabits'] = {'$lt':0}

    #ditto (0: smokes sometimes, 1: smokes regularly)
    if currentUser['preferences']['smokingPref'] == ABSYES:
        livingQuery['smokingHabits'] = {'$gte':0}
    elif currentUser['preferences']['smokingPref'] == ABSNO:
        livingQuery['smokingHabits'] = {'$lt':0}

    #and so on
    if currentUser['preferences']['drinkingPref'] == ABSYES:
        livingQuery['drinkingHabits'] = {'$gte':0}
    elif currentUser['preferences']['drinkingPref'] == ABSNO:
        livingQuery['drinkingHabits'] = {'$lt':0}

    if currentUser['preferences']['drugsPref'] == ABSYES:
        livingQuery['drugsHabits'] = {'$gte':0}
    elif currentUser['preferences']['drugsPref'] == ABSNO:
        livingQuery['drugsHabits'] = {'$lt':0}

    if currentUser['preferences']['guestsPref'] == ABSYES:
        livingQuery['guestsHabits'] = {'$gte':0}
    elif currentUser['preferences']['guestsPref'] == ABSNO:
        livingQuery['guestsHabits'] = {'$lt':0}

    #query for the fields that are in preferences
    preferenceQuery = {'maxBudget':{'$lte': currentUser['preferences']['maxBudget']*(1+BUDGETTOLERANCE)}}

    #livingpref is reflexive
    if currentUser['preferences']['livingPref'] == ABSYES:
        preferenceQuery['livingPref'] = {'$gte':0}
    elif currentUser['preferences']['livingPref'] == ABSNO:
        preferenceQuery['livingPref'] = {'$lt':0}
    
    if currentUser['preferences']['livingPref'] >= 0:
        preferenceQuery['livingPref'] = {'$ne':ABSNO}
    else:
        preferenceQuery['livingPref'] = {'$ne':ABSYES}

    
    #Example of PART 2 query: if current user has pets, do not get users from database who absolutely do NOT want to live with pets
    if currentUser['livingData']['hasPets']:
        preferenceQuery['petsPref'] = {'$ne':ABSNO}
    else:
        preferenceQuery['petsPref'] = {'$ne':ABSYES}

    #ditto with children
    if currentUser['livingData']['hasChildren']:
        preferenceQuery['childrenPref'] = {'$ne':ABSNO}
    else:
        preferenceQuery['childrenPref'] = {'$ne':ABSYES}

    #ditto
    if currentUser['livingData']['cleaningHabits'] >= 0:
        preferenceQuery['cleaningPref'] = {'$ne':ABSNO}
    else:
        preferenceQuery['cleaningPref'] = {'$ne':ABSYES}

    if currentUser['livingData']['drinkingHabits'] >= 0:
        preferenceQuery['drinkingPref'] = {'$ne':ABSNO}
    else:
        preferenceQuery['drinkingPref'] = {'$ne':ABSYES}

    if currentUser['livingData']['smokingHabits'] >= 0:
        preferenceQuery['smokingPref'] = {'$ne':ABSNO}
    else:
        preferenceQuery['smokingPref'] = {'$ne':ABSYES}

    if currentUser['livingData']['drugsHabits'] >= 0:
        preferenceQuery['drugsPref'] = {'$ne':ABSNO}
    else:
        preferenceQuery['drugsPref'] = {'$ne':ABSYES}

    if currentUser['livingData']['guestsHabits'] >= 0:
        preferenceQuery['guestsPref'] = {'$ne':ABSNO}
    else:
        preferenceQuery['guestsPref'] = {'$ne':ABSYES}



    #queries in $or for PART 2 "same X" preferences
    orQueries = []

    # Only get from the database users who either absolutely want to live with people of the same occupation and have the same occupation as the current user,
    # OR absolutely do NOT want to live with people of the same occupation and have a different occupation than the current user,
    # OR do not have a strong preference about occupation
    orQueries.append(
        {
            '$or':[
            {'profile.preferences.sameOccupationPref':ABSYES,
            'profile.livingData.occupation':currentUser['livingData']['occupation']},
            {'profile.preferences.sameOccupationPref':ABSNO,
            'profile.livingData.occupation':{'$ne':currentUser['livingData']['occupation']}},
            {'profile.preferences.sameOccupationPref':{'$nin':[ABSYES, ABSNO]}}
        ]
        }
    )
    #Ditto with age (more complicated due to thresholds)
    orQueries.append(
        {
            '$or':[
            {'profile.preferences.sameAgePrefPref':ABSYES,
            'profile.personalData.age':{'$gte': currentUser['personalData']['age']-AGEWINDOW, '$lte':currentUser['personalData']['age']+AGEWINDOW}},
            {'profile.preferences.sameAgePrefPref':ABSNO,
            'profile.personalData.age':{'$lte': currentUser['personalData']['age']-AGEWINDOW, '$gte':currentUser['personalData']['age']+AGEWINDOW}},
            {'profile.preferences.sameAgePrefPref':{'$nin':[ABSYES, ABSNO]}}
        ]
        }
    )
    #ditto
    orQueries.append(
        {
            '$or':[
            {'profile.preferences.sameGenderPrefPref':ABSYES,
            'profile.personalData.gender':currentUser['personalData']['gender']},
            {'profile.preferences.sameGenderPrefPref':ABSNO,
            'profile.personalData.gender':{'$ne':currentUser['personalData']['gender']}},
            {'profile.preferences.sameGenderPrefPref':{'$nin':[ABSYES, ABSNO]}}
        ]
        }
    )

    orQueries.append(
        {
            '$or':[
            {'profile.preferences.sameNationalityPrefPref':ABSYES,
            'profile.personalData.nationality.code':currentUser['personalData']['nationality']['code']},
            {'profile.preferences.sameNationalityPrefPref':ABSNO,
            'profile.personalData.nationality.code':{'$ne':currentUser['personalData']['nationality']['code']}},
            {'profile.preferences.sameNationalityPrefPref':{'$nin':[ABSYES, ABSNO]}}
        ]
        }
    )

    #Compile a single query
    for queryfield in personalQuery:
        query[f'profile.personalData.{queryfield}'] = personalQuery[queryfield]
    for queryfield in livingQuery:
        query[f'profile.livingData.{queryfield}'] = livingQuery[queryfield]
    for queryfield in preferenceQuery:
        query[f'profile.preferences.{queryfield}'] = preferenceQuery[queryfield]
    query['$and'] = orQueries

    #Get candidates
    results = userCollection.find(query)
    
    #PART 3 computes all compatibility scores between the current user and the candidates.
    computeScores(cu, results, matchesCollection)


def computeScores(cu, otherUsers, matchesCollection):
    
    currentUser = cu['profile']
    
    for ou in otherUsers:
        
        otherUser = ou['profile']

        #Compute score one way (desireability of roommates is not necessarily symmetric)
        currentToOther = 0

        if otherUser['preferences']['maxBudget'] <= currentUser['preferences']['maxBudget'] and otherUser['preferences']['maxBudget'] >= currentUser['preferences']['maxBudget']*(1-BUDGETTOLERANCE):
            currentToOther += 1


        # Add partial scores based on compatibility of values
        # If a user has a preference of 0 ("don't care") then add a point by default (if you have no preference you match with anyone).
        # Otherwise, partial score is the product of one user's preference with the other's habit. Positive preferences (e.g. 1: prefer drinkers)
        # times positive habits (e.g. 1: does drink) give a positive score (e.g. +1), and so do negative preferences times negative habits.
        # If there is a mismatch in sign (e.g. 1: prefer drink and -1: does not drink) the resulting partial score is negative (e.g. -1).
        
        if currentUser['preferences']['smokingPref'] == 0:
            currentToOther += 1
        else:
            currentToOther += currentUser['preferences']['smokingPref']*otherUser['livingData']['smokingHabits']

        #All score computations are basically the same
        if currentUser['preferences']['cleaningPref'] == 0:
            currentToOther += 1
        else:
            currentToOther += currentUser['preferences']['cleaningPref']*otherUser['livingData']['cleaningHabits']

        if currentUser['preferences']['drinkingPref'] == 0:
            currentToOther += 1
        else:
            currentToOther += currentUser['preferences']['drinkingPref']*otherUser['livingData']['drinkingHabits']

        if currentUser['preferences']['drugsPref'] == 0:
            currentToOther += 1
        else:
            currentToOther += currentUser['preferences']['drugsPref']*otherUser['livingData']['drugsHabits']

        if currentUser['preferences']['guestsPref'] == 0:
            currentToOther += 1
        else:
            currentToOther += currentUser['preferences']['guestsPref']*otherUser['livingData']['guestsHabits']



        if currentUser['preferences']['livingPref'] == 0:
            currentToOther += 1
        elif currentUser['preferences']['livingPref'] == otherUser['preferences']['livingPref']:
            currentToOther += 1
        else:
            currentToOther += 0



        if currentUser['preferences']['childrenPref'] == 0:
            currentToOther += 1
        elif otherUser['livingData']['hasChildren']:
            currentToOther += currentUser['preferences']['childrenPref']
        else:
            currentToOther -= currentUser['preferences']['childrenPref']


        if currentUser['preferences']['petsPref'] == 0:
            currentToOther += 1
        elif otherUser['livingData']['hasPets']:
            currentToOther += currentUser['preferences']['petsPref']
        else:
            currentToOther -= currentUser['preferences']['petsPref']


        if currentUser['preferences']['sameAgePref'] == 0:
            currentToOther += 1
        elif otherUser['personalData']['age'] <= currentUser['personalData']['age'] + AGEWINDOW and otherUser['personalData']['age'] >= currentUser['personalData']['age'] - AGEWINDOW:
            currentToOther += currentUser['preferences']['sameAgePref']
        else:
            currentToOther -= currentUser['preferences']['sameAgePref']

        if currentUser['preferences']['sameGenderPref'] == 0:
            currentToOther += 1
        elif otherUser['personalData']['gender'] == currentUser['personalData']['gender']:
            currentToOther += currentUser['preferences']['sameGenderPref']
        else:
            currentToOther -= currentUser['preferences']['sameGenderPref']

        if currentUser['preferences']['sameOccupationPref'] == 0:
            currentToOther += 1
        elif otherUser['livingData']['occupation'] == currentUser['livingData']['occupation']:
            currentToOther += currentUser['preferences']['sameOccupationPref']
        else:
            currentToOther -= currentUser['preferences']['sameOccupationPref']

        if currentUser['preferences']['sameNationalityPref'] == 0:
            currentToOther += 1
        elif otherUser['personalData']['nationality']['code'] == currentUser['personalData']['nationality']['code']:
            currentToOther += currentUser['preferences']['sameNationalityPref']
        else:
            currentToOther -= currentUser['preferences']['sameNationalityPref']



        #Do the same computation in the other direction (other user to current user)

        otherToCurrent = 0

        #add partial scores based on compatibility of values
        if currentUser['preferences']['maxBudget'] <= otherUser['preferences']['maxBudget'] and currentUser['preferences']['maxBudget'] >= otherUser['preferences']['maxBudget']*(1-BUDGETTOLERANCE):
            currentToOther += 1


        if otherUser['preferences']['smokingPref'] == 0:
            otherToCurrent += 1
        else:
            otherToCurrent += otherUser['preferences']['smokingPref']*currentUser['livingData']['smokingHabits']
        
        if otherUser['preferences']['guestsPref'] == 0:
            otherToCurrent += 1
        else:
            otherToCurrent += otherUser['preferences']['guestsPref']*currentUser['livingData']['guestsHabits']

        if otherUser['preferences']['drinkingPref'] == 0:
            otherToCurrent += 1
        else:
            otherToCurrent += otherUser['preferences']['drinkingPref']*currentUser['livingData']['drinkingHabits']

        if otherUser['preferences']['drugsPref'] == 0:
            otherToCurrent += 1
        else:
            otherToCurrent += otherUser['preferences']['drugsPref']*currentUser['livingData']['drugsHabits']

        if otherUser['preferences']['cleaningPref'] == 0:
            otherToCurrent += 1
        else:
            otherToCurrent += otherUser['preferences']['cleaningPref']*currentUser['livingData']['cleaningHabits']


        if otherUser['preferences']['livingPref'] == 0:
            otherToCurrent += 1
        elif otherUser['preferences']['livingPref'] == currentUser['preferences']['livingPref']:
            otherToCurrent += 1
        else:
            otherToCurrent += 0


        if otherUser['preferences']['childrenPref'] == 0:
            otherToCurrent += 1
        elif currentUser['livingData']['hasChildren']:
            otherToCurrent += otherUser['preferences']['childrenPref']
        else:
            otherToCurrent -= otherUser['preferences']['childrenPref']


        if otherUser['preferences']['petsPref'] == 0:
            otherToCurrent += 1
        elif currentUser['livingData']['hasPets']:
            otherToCurrent += otherUser['preferences']['petsPref']
        else:
            otherToCurrent -= otherUser['preferences']['petsPref']

        if otherUser['preferences']['sameGenderPref'] == 0:
            otherToCurrent += 1
        elif currentUser['personalData']['gender'] == otherUser['personalData']['gender']:
            otherToCurrent += otherUser['preferences']['sameGenderPref']
        else:
            otherToCurrent -= otherUser['preferences']['sameGenderPref']

        if otherUser['preferences']['sameOccupationPref'] == 0:
            otherToCurrent += 1
        elif currentUser['livingData']['occupation'] == otherUser['livingData']['occupation']:
            otherToCurrent += otherUser['preferences']['sameOccupationPref']
        else:
            otherToCurrent -= otherUser['preferences']['sameOccupationPref']

        if otherUser['preferences']['sameNationalityPref'] == 0:
            otherToCurrent += 1
        elif currentUser['personalData']['nationality']['code'] == otherUser['personalData']['nationality']['code']:
            otherToCurrent += otherUser['preferences']['sameNationalityPref']
        else:
            otherToCurrent -= otherUser['preferences']['sameNationalityPref']

        actualScore = min(currentToOther, otherToCurrent)

        #symmetric parameters only need to be computed once
        if currentUser['personalData']['personality'] == otherUser['personalData']['personality']:
            actualScore += 2

        #If users share common traits or have common interests, that can contribute to their compatibility score...

        commonTags = [tag for tag in currentUser['extraData']['personalTags'] if tag in otherUser['extraData']['personalTags']]
        commonInterests = [tag for tag in currentUser['extraData']['interestsTags'] if tag in otherUser['extraData']['interestsTags']]
        commonalities = len(commonTags)+len(commonInterests)

        # ...but only up to 4 points
        actualScore += min(commonalities, 4)



        finalScore = min(currentToOther, otherToCurrent) #least common score is the actual one
        #trim edges
        finalScore = min(finalScore, MAXSCORE)
        finalScore = max(finalScore, MINSCORE)
        #normalize
        finalScore = (finalScore - MINSCORE)/(MAXSCORE - MINSCORE)
        finalScore = round(finalScore*100)
        
        #define an order (avoids having multiple match objects for the same pair)
        u1 = min(cu['userId'], ou['userId'])
        u2 = max(cu['userId'], ou['userId'])

        #Insert the newly computed match into the database (update if a match already exists)
        matchesCollection.replace_one({'u1':u1, 'u2': u2}, {'u1':u1, 'u2': u2, 'score' : finalScore}, True)