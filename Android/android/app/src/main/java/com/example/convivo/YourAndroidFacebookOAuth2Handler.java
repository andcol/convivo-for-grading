package com.example.convivo;

import android.app.Activity;

import com.byteowls.capacitor.oauth2.handler.AccessTokenCallback;
import com.byteowls.capacitor.oauth2.handler.OAuth2CustomHandler;
//import com.byteowls.teamconductor.MainActivity;
import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.getcapacitor.PluginCall;

public class YourAndroidFacebookOAuth2Handler implements OAuth2CustomHandler {

    @Override
    public void getAccessToken(Activity activity, PluginCall pluginCall, final AccessTokenCallback callback) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (AccessToken.isCurrentAccessTokenActive()) {
            callback.onSuccess(accessToken.getToken());
        } else {
            LoginManager.getInstance().logInWithReadPermissions(activity, null);

            LoginManager.getInstance().registerCallback(((MainActivity) activity).getCallbackManager(), new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    callback.onSuccess(loginResult.getAccessToken().getToken());
                }

                @Override
                public void onCancel() {
                    callback.onCancel();
                }

                @Override
                public void onError(FacebookException error) {
                    callback.onCancel();
                }
            });
        }

    }

    @Override
    public boolean logout(PluginCall pluginCall) {
        LoginManager.getInstance().logOut();
        return true;
    }

}