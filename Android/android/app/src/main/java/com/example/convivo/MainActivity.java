package com.example.convivo;

import android.content.Intent;
import android.os.Bundle;

import com.byteowls.capacitor.oauth2.OAuth2ClientPlugin;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.getcapacitor.BridgeActivity;
import com.getcapacitor.Plugin;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BridgeActivity {

  private CallbackManager callbackManager;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    FacebookSdk.sdkInitialize(this.getApplicationContext());

    callbackManager = CallbackManager.Factory.create();

    // add my plugins here
    List<Class<? extends Plugin>> additionalPlugins = new ArrayList<>();
    // Additional plugins you've installed go here
    additionalPlugins.add(OAuth2ClientPlugin.class);
    // Ex: additionalPlugins.add(TotallyAwesomePlugin.class);

    // Initializes the Bridge
    this.init(savedInstanceState, additionalPlugins);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (callbackManager.onActivityResult(requestCode, resultCode, data)) {
      return;
    }
  }

  public CallbackManager getCallbackManager() {
    return callbackManager;
  }

}
