import React, { useState, useEffect } from 'react';
import './App.css';
import { AppBar, Toolbar, IconButton, Typography, Button, SvgIcon } from '@material-ui/core';
import {Switch, Route} from 'react-router-dom';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles'
import CreateProfile from './components/CreateProfile/CreateProfile';
import MainPage from './components/MainPage';
import { ThemeProvider } from '@material-ui/styles';
import {Redirect, useHistory} from 'react-router-dom'
import LoginScreen from './components/Login/LoginScreen';
import SplashScreen from './components/Login/SplashScreen';
import UserTools from './components/UserTools';
import {registerWebPlugin} from "@capacitor/core";
import {OAuth2Client} from '@byteowls/capacitor-oauth2';
import { makeProfilePhotoUrl } from './lib/lib';
import { CONVIVO_ICON } from './constants';
import Notificator from './components/Notificator';
import { useCookies } from 'react-cookie';

const HOUR_IN_SECONDS = 3600;

const theme = createMuiTheme({
  palette: {
      primary: {
          main: "#334477"
      },
      secondary: {
          main: "#ff9900"
      }
    },
  
  flexBox: {
    display: 'flex'
  }  
});

const useStyles = makeStyles(theme => ({
  view: {
    marginTop: 100
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    textAlign: 'left',
    flexGrow: 1,
  },
}));

/** Main component. Orchestrates all the views and offers the top navbar. */

function App() {
  const [userData, setUserData] = useState(null); //Keeps trac of current user data (e.g. current token, name, email, etc.)
  const [avatarUrl, setAvatarUrl] = useState('/res/placeholder.png');

  const [cookies, setCookies, removeCookies] = useCookies(['userData']); //userData is stored in cookies to prevent logging in every time

  const setUserProfile = (profile) => {
    setUserData(ud => ({...ud, profile:profile, notifications:[]}));
  }

  const classes = useStyles();
  const history = useHistory();

  const isAuthenticated =  userData !== null;

  const isAuthorized =  userData !== null && userData.accessToken !== null;

  const handleLogout = () => {
    removeCookies('userData');
    setUserData(null);
  }

  const invalidateToken = () => {
    setUserData(ud => ({...ud, accessToken: null}));
  }

  useEffect(() => {
    registerWebPlugin(OAuth2Client);
  },[])

  useEffect(() => {
    if(cookies.userData !== undefined) {
      setUserData(cookies.userData);
    }
  },[])

  useEffect(() => {
    const updateAvatar = async () => {
      const ProfilePhotoUrl = await makeProfilePhotoUrl(userData.userId);
      setAvatarUrl(ProfilePhotoUrl);
    }

    if (isAuthorized) {
      setCookies('userData', userData, {maxAge: HOUR_IN_SECONDS});
      updateAvatar();
      if(userData.profile === null) {
        //Signup case, redirect to profile creation
        history.push('/new');
      } else {
        if(history.location.pathname.endsWith('/new')) {
          //profile creation was completed, go to homepage
          history.push('/')
        } else {
          //login was completed after some page was denied access due to inauthenticated or unauthorized user, go back to that page
          history.goBack();
        }
      }
    }
    //if unauthorized or inauthenticated, re-routing is taken care of by PrivateRoute
    console.log(userData);
  }, [userData])

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <AppBar position="fixed">
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
            <SvgIcon><path d={CONVIVO_ICON}/></SvgIcon>
              &nbsp;
              Convivo
            </Typography>
            <Notificator userData={userData}/>
            <UserTools avatarUrl={avatarUrl} authenticated={isAuthenticated} handleLogout={handleLogout}/>
          </Toolbar>
        </AppBar>

        <div className={classes.view}>
        <Switch>
          <Route exact path='/welcome'>
            <SplashScreen passData={setUserData}/>
          </Route>
          <Route exact path='/login'>
            <LoginScreen passData={setUserData}/>
          </Route>
          <PrivateRoute exact path='/' authenticated={isAuthenticated} authorized={isAuthorized}>
            <MainPage userData={userData} invalidateToken={invalidateToken}/>
          </PrivateRoute>
          <PrivateRoute path="/new" authenticated={isAuthenticated} authorized={isAuthorized}>
            <CreateProfile userData={userData} setUserProfile={setUserProfile} handleLogout={handleLogout}/>
          </PrivateRoute>
          
        </Switch>
        </div>

      </div>
    </ThemeProvider>
  );
}

/** Wrapper component for React Router's Route.
 * If user is authenticated and authorized, display the requested page
 * If user is unauthorized, redirect to the login page
 * If user is inauthenticated, redirect to the welcome screen
 */
function PrivateRoute({ authenticated, authorized, children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        authorized ? (
          children
        ) : authenticated ? (
          <Redirect push
            to='/login'
          />
        ) : <Redirect push
              to='/welcome'
            />
      }
    />
  );
}

export default App;
