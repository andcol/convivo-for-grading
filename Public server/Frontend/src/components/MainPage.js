import React, { useState, useEffect } from 'react'
import { makeStyles, useMediaQuery, useTheme } from '@material-ui/core';
import {API_MATCHES_ENDPOINT} from '../constants.js'
import ProfileDetails from './ViewProfile/ProfileDetails';
import ProfilePreview from './ViewProfile/ProfilePreview.js';

const useStyles = makeStyles(theme => ({
    profilesBox: {
        display:'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        margin: 'auto',
        width: '80%',
        padding: 30,
        justifyContent: 'center',
        [theme.breakpoints.down('sm')]: {
            padding: 0,
            width: '100%'
        },
    }
}));

/** This component handles everything that happens in the main page:
 *      > Getting match data
 *      > Profile previews
 *      > Opening profile details
 */

export default function MainPage(props) {

    const {userData, invalidateToken} = props;

    const [matches, setMatches] = useState([]);
    const [currentlyViewing, setCurrentlyViewing] = useState(null);

    const theme = useTheme();
    const classes =  useStyles();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    useEffect(() => {
        async function doLoadData() {
            const req = await fetch(API_MATCHES_ENDPOINT + '/' + userData.userId, {
                headers: {
                    'Authorization': 'Bearer ' + userData.accessToken
                }
            });

            if (req.status == 200) {
                const resp = await req.json();
                setMatches(resp.matches);
            } else {
                //The user is either unauthorized or does not have a profile
                invalidateToken();
            }
        }

        doLoadData();
    }, []);

    const handleOpen = id => () => {
        setCurrentlyViewing(matches[id]);
    }

    const handleClose = () => {
        setCurrentlyViewing(null);
    }

    return (
        <div className={classes.profilesBox}>
            {matches.map((m, i) =>
                <ProfilePreview key={m.userId} match={m} clickCallback={handleOpen(i)}/>
            )}
            <ProfileDetails userData={userData} match={currentlyViewing} fullScreen={fullScreen} handleClose={handleClose}/>
        </div>
    )
}
