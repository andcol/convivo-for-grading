import React, { useEffect, useState } from 'react'
import { CardActionArea, Card, CardMedia, Typography, CardContent, Box, useTheme, useMediaQuery } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import FlagIcon from '../FlagIcon'
import { makeProfilePhotoUrl } from '../../lib/lib';


const useStyles = makeStyles(theme => (
    {
        profileCard: {
            height: 300,
            width: 200,
            margin: 10,
            [theme.breakpoints.down('sm')]: {
                height: 300,
                width: '45%',
                margin: 5
            },
        },
        descriptionBox: {
            display: 'flex',
            flexDirection: 'column',
            textAlign:'left',
            justifyContent:'space-between',
            [theme.breakpoints.down('sm')]: {
            },
        }
    }
));

/** This component renders the user profile card seen in the main page */

export default function ProfilePreview(props) {
    const { match, clickCallback } = props;

    const [avatarUrl, setAvatarUrl] = useState('/res/placeholder.png');

    const score = match.score;
    const userId = match.userId;
    const profile = match.profile;
    const shortName = profile.displayName.slice(0,6) + (profile.displayName.length > 6 ? '...' : '');

    const classes = useStyles();

    const theme = useTheme();
    const mobile = useMediaQuery(theme.breakpoints.down('sm'));

    useEffect(() => {
        const updateAvatar = async () => {
            const ProfilePhotoUrl = await makeProfilePhotoUrl(userId);
            setAvatarUrl(ProfilePhotoUrl);
          }
        
        updateAvatar();
    },[])

    return (
        <Card className={classes.profileCard}>
            <CardActionArea onClick={clickCallback}>
                <CardMedia
                component="img"
                height={200}
                image={avatarUrl}
                />
                <CardContent>
                    <Box className={classes.descriptionBox}>
                        <Typography gutterBottom variant={mobile ? 'body2' : "h6"} component="h2">
                            <FlagIcon code={profile.personalData.nationality.code}/>
                            &nbsp;&nbsp;
                            {shortName}, {profile.personalData.age}
                        </Typography>
                        <Typography gutterBottom variant={mobile ? 'body2' : 'body2'} component='p'>
                            {score}% compatibility
                        </Typography>
                    </Box>
                </CardContent>
            </CardActionArea>
        </Card>
    )
}
