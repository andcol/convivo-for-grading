import React, { useEffect, useState } from 'react'
import { Dialog, DialogTitle, DialogContent, Table, TableRow, TableCell, DialogActions, Button, TableBody, Slide, Zoom, Avatar, Typography, IconButton, Chip, Box, SvgIcon } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import Close from '@material-ui/icons/Close';
import { FB_ICON, TWITTER_ICON, IG_ICON, FB_URL, TWITTER_URL, IG_URL, API_USERS_ENDPOINT } from '../../constants';
import { makeProfilePhotoUrl } from '../../lib/lib';
import Notification from '../Notification';

//Mapping from field names to human-readable field descriptions
const mapFieldNames = {
    'age' : 'Age',
    'gender' : 'Gender',
    'f' : 'Female',
    'm' : 'Male',
    'n' : 'Non-binary',
    'u' : 'Prefer not to say',
    'nationality' : 'Nationality',
    'personality' : 'Personality',
    'i' : 'Introvert',
    'e' : 'Extrovert',
    'occupation': 'Occupation',
    'hasPets': 'Pets',
    'hasChildren': 'Children',
    'cleaningHabits': 'Cleans',
    'drinkingHabits': 'Drinks',
    'smokingHabits': 'Smokes',
    'drugsHabits': 'Uses substances',
    'guestsHabits': 'Has guests',
    'student': 'Student',
    'worker': 'Worker',
    'nightWorker': 'Worker (night shift)'
}




const display = code => {
    if (typeof code === 'object') {
        //case of Nationality
        return code.name;
    } else if (mapFieldNames[code]) {
        return mapFieldNames[code];
    } else if (code === true) {
        return 'Yes';
    } else if (code === false) {
        return 'No';
    } else if (code === -1) {
        return 'Never'
    } else if (code === 0) {
        return 'Rarely'
    } else if (code === 1) {
        return 'Yes'
    } else {
        return code;
    }
}

const useStyles = makeStyles(theme => ({
    dialogTitle: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    avatar: {
        width: '3em',
        height: '3em',
        marginRight: theme.spacing(2)
    },
    socialMediaBox: {
        padding: 30,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        '& a':{
            marginLeft: 3,
            marginRight: 3
        }
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1)
    },
    tag: {
        marginRight: theme.spacing(1),
        marginBottom: theme.spacing(1)
    }
}))

/** This component renders the dialog with all the user profile information */

export default function ProfileDetails(props) {
    const {userData, match, fullScreen, handleClose} = props;

    const [avatarUrl, setAvatarUrl] = useState('/res/placeholder.png');
    const [openNotification, setOpenNotification] = useState(false);

    const score = match ? match.score : 0;
    const userId = match? match.userId : null;
    const name = match ? match.profile.displayName : '';
    const bio = match ? match.profile.bio : '';
    const tags = match ? match.profile.extraData.personalTags : [];
    const interests = match ? match.profile.extraData.interestsTags : [];
    const personalData = match ? match.profile.personalData : {};
    const livingData = match ? match.profile.livingData : {};
    const socialMedia = match ? match.profile.socialMedia : {};

    const classes = useStyles();

    const handleGetInTouch = () => async e => {
        //Post a request notification to the user of interest
        const request = await fetch(API_USERS_ENDPOINT + '/' + userId + '/notifications', {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + userData.accessToken,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userId: userData.userId,
                type: 'request'
            })
        });
        if (request.status == 200) {
            console.log('yay');
            //Notify the user of the successful request
            setOpenNotification(true);
        } else {
            console.log('nay');
        }
    }

    useEffect(() => {
        const updateAvatar = async () => {
            const ProfilePhotoUrl = await makeProfilePhotoUrl(userId);
            setAvatarUrl(ProfilePhotoUrl);
          }
        
        updateAvatar();
    },[match])

    return (
        <Dialog
            fullScreen={fullScreen}
            maxWidth='sm'
            fullWidth
            open={match != null}
            TransitionComponent={Zoom}
            keepMounted
            onClose={handleClose}
        >
            <DialogTitle className={classes.dialogTitle} disableTypography>
                <Avatar src={avatarUrl} className={classes.avatar}/>
                <div>
                    <Typography variant='h5'>{name}</Typography>
                    <Typography variant='body1'>{score}% compatibility</Typography>
                </div>
                <IconButton aria-label="close" onClick={handleClose} className={classes.closeButton}>
                 <Close/>
                </IconButton>   
            </DialogTitle>
            <DialogContent>
                {bio &&
                    <div>
                        <Typography variant='body2' color='secondary' gutterBottom>&nbsp;Bio</Typography>
                        <Typography variant='body1' gutterBottom>
                            {bio}
                        </Typography>
                    </div>
                }
                {tags.length>0 &&
                    <div>
                        <Typography variant='body2' color='secondary' gutterBottom>&nbsp;Tags</Typography>
                        <Typography gutterBottom>
                            {tags.map(t => <Chip className={classes.tag} label={t} onClick={()=>{}}/>)}
                        </Typography>
                    </div>
                }
                {interests.length>0 &&
                    <div>
                        <Typography variant='body2' color='secondary' gutterBottom>&nbsp;Interested in</Typography>
                        <Typography gutterBottom>
                            {interests.map(i => <Chip className={classes.tag} label={i} onClick={()=>{}}/>)}
                        </Typography>
                    </div>
                }

                <Typography variant='body2' color='secondary' gutterBottom>&nbsp;User data</Typography>
                <Table>
                    <TableBody>
                    {
                        Object.keys(personalData).map(k => (
                            <TableRow><TableCell>{display(k)}</TableCell><TableCell>{display(personalData[k])}</TableCell></TableRow>
                        ))
                    }
                    {
                        Object.keys(livingData).map(k => (
                            <TableRow><TableCell>{display(k)}</TableCell><TableCell>{display(livingData[k])}</TableCell></TableRow>
                        ))
                    }
                    </TableBody>
                </Table>
                <Box className={classes.socialMediaBox}>
                    {socialMedia.facebook &&
                        <a target="_blank" href={FB_URL+socialMedia.facebook}><SvgIcon><path d={FB_ICON}/></SvgIcon></a>
                    }
                    {socialMedia.twitter &&
                        <a target="_blank" href={TWITTER_URL+socialMedia.twitter}><SvgIcon><path d={TWITTER_ICON}/></SvgIcon></a>
                    }
                    {socialMedia.instagram &&
                        <a target="_blank" href={IG_URL+socialMedia.instagram}><SvgIcon><path d={IG_ICON}/></SvgIcon></a>
                    }
                </Box>
            </DialogContent>
            <DialogActions>
            <Button onClick={handleGetInTouch()} color="primary">
                Get in touch
            </Button>
            </DialogActions>
            <Notification msg="Request sent." trigger={openNotification} setTrigger={setOpenNotification}/>
        </Dialog>
        
    )
}
