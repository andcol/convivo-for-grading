import React, { useState, useEffect } from 'react'

import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { IconButton, Menu, MenuItem, Divider, Avatar, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { makeProfilePhotoUrl } from '../lib/lib'
import { API_USERS_ENDPOINT, API_MATCHES_ENDPOINT } from '../constants';
import Notification from './Notification';

const useStyles = makeStyles(theme => ({
    root: {
    position: 'fixed',
    width: '100%',
    maxWidth: 360,
    color: 'black',
    backgroundColor: theme.palette.background.paper,
    },
    notificationBox: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    avatar: {
        marginRight: 10
    },
    textandbutton: {
        display:'flex',
        flexDirection:'row',
        alignItems:'baseline',
        justifyContent:'space-between'
    }
  }));

  /** Handles notification display and their interaction*/

export default function Notificator(props) {
    const {userData} = props;

    const notifications = userData ? userData.notifications : null;
    const userProfile = userData ? userData.profile : null;

    //const [profiles, setProfiles] = useState([]); //No longer used
    const [matches, setMatches] = useState([])
    const [avatars, setAvatars] = useState([])

    const [unread, setUnread] = useState(true);
    const [openNotification, setOpenNotification] = useState(false);
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    const classes = useStyles();

    const handleNotification = event => {
        if (notifications.length > 0) {
            setUnread(false);
            setAnchorEl(event.currentTarget);
        }
    }

    const handleClose = () => {
        setAnchorEl(null);
    }

    useEffect (() => {
        console.log(notifications, avatars, matches);
    },[notifications]);

    const makeHandleAccept = userid => async e => {
        //Post a notification on the user that was accepted
        const request = await fetch(API_USERS_ENDPOINT + '/' + userid + '/notifications', {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + userData.accessToken,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userId: userData.userId,
                type: 'accepted'
            })
        });
        if(request.status == 200) {
            console.log('yay');
            //Notify the current user of the success
            setOpenNotification(true);
        } else {
            console.log('nay');
        }
    }
    
    useEffect(() => {
        const updateMatches = async () => {
            const newMatches = []
            for (let i = 0; i < notifications.length; i++) {
                var score = 0;
                if(notifications[i].type !== 'welcome') {
                    //For request notifications, get relevant user data (could be done away with by chaning the notifications API)
                    score = await fetch(API_MATCHES_ENDPOINT + '/' + userData.userId + '/' + notifications[i].userId, {
                        headers: {
                            'Authorization': 'Bearer ' + userData.accessToken
                        }});
                    if (score.status == 200) {
                        const payload = await score.json();
                        newMatches.push(payload);
                    } else {
                        newMatches.push(null);
                    }
                } else {
                    newMatches.push(null);
                }
            }
            setMatches(newMatches);
        }

        const updateAvatars = async () => {
            //Get pics for notifications
            const profilePhotoUrls = []
            for (let n of notifications) {
                var profilePhotoUrl = ''
                if(n.type == 'welcome') {
                    profilePhotoUrl = '/res/convivo-dark-full.png'
                } else {
                    profilePhotoUrl = await makeProfilePhotoUrl(n.userId);
                }
                profilePhotoUrls.push(profilePhotoUrl);
            }
            setAvatars(profilePhotoUrls);
          }

        /** No longer used
            const updateProfiles = async () => {
                const userProfiles = []
                for (let i = 0; i < notifications.length; i++) {
                    if(notifications[i].type !== 'welcome') {
                        const profile = await fetch(API_USERS_ENDPOINT + '/' + notifications[i].userId + '/profile', {
                            headers: {
                                'Authorization': 'Bearer ' + userData.accessToken
                            }});
                        if (profile.status == 200) {
                            const payload = await profile.json();
                            userProfiles.push(payload);
                        } else {
                            userProfiles.push(null);
                        }
                    } else {
                        userProfiles.push(null);
                    }
                }
                setProfiles(userProfiles);
            }
        */
        
        if (notifications !== null) {
            updateMatches();
            //updateProfiles(); //no longer used
            updateAvatars();
        }
    },[notifications]);

    return (
        <div>
            {userProfile &&
                <div>
                    <IconButton
                    aria-label="Notifications"
                    aria-controls="menu-appbar"
                    aria-haspopup="true"
                    onClick={handleNotification}
                    color="inherit"
                >
                    {notifications.length > 0 && unread
                    ? <NotificationsIcon/>
                    : <NotificationsNoneIcon/>}
                </IconButton>
                <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                    }}
                    keepMounted
                    transformOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                    }}
                    open={open}
                    onClose={handleClose}
                >
                    {matches.length>0 && matches.map((m,i) => 
                        [<MenuItem className={classes.notificationBox}>
                            <Avatar src={avatars[i]} className={classes.avatar}/>
                            <div>
                                {notifications[i].type == 'request' &&
                                    <div>
                                        <Typography variant='body2'>{m.profile.displayName} wants to be your roommate!</Typography>
                                        <div className={classes.textandbutton}>
                                            <Typography variant='body2'>{m.score}% compatibility</Typography>
                                            <Button color='secondary' onClick={makeHandleAccept(m.userId)}>Accept</Button>
                                        </div>
                                    </div>
                                }
                                {notifications[i].type == 'accepted' &&
                                    <div>
                                        <Typography variant='body2'>{m.profile.displayName} accepted to be your roommate!</Typography>
                                        <div className={classes.textandbutton}>
                                            <Typography variant='body2'>Check your email for further details.</Typography>
                                        </div>
                                    </div>
                                }
                                {notifications[i].type == 'welcome' &&
                                    <div>
                                        <Typography variant='body2'>Welcome to Convivo!</Typography>
                                    </div>
                                }
                            </div>
                        </MenuItem>,
                        <Divider/>]
                    )}
                </Menu>
                <Notification msg="It's a match! Check your email for further information." trigger={openNotification} setTrigger={setOpenNotification}/>
                </div>
            }
        </div>
    )
}
