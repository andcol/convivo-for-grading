import React, { useState } from 'react';
import {Plugins} from '@capacitor/core'
import {API_USERS_ENDPOINT, FACEBOOK_ID, FACEBOOK_CALLBACK, API_AUTH_ENDPOINT} from '../../constants.js'
import { makeStyles } from '@material-ui/styles';
import { Typography, Button, Box, TextField } from '@material-ui/core';
import ErrorIcon from '@material-ui/icons/Error'

const useStyles = makeStyles(theme => ({
    loginbutton: {
        color: '#fff',
        minWidth: 300,
        minHeight: 60,
        margin: 10,
        border: 0,
        borderRadius: 30,
        cursor: 'pointer'
        
    },
    facebook: {
        backgroundColor: '#3b5998',
        '&:hover': {
            backgroundColor: '#395796'
         }
    },
    google: {
        backgroundColor: '#de5246'
    },
    errorBox: {
        justifyContent: 'center',
        display: 'flex',
        alignItems: 'center'
    },
}));

/** This component handles the login form and login functions */

export default function Login(props) {

    const {passData} = props;

    const [form, setForm] = useState({
        User: '',
        Password: ''
    });
    const [error, setError] = useState(false);

    const classes = useStyles();

    /** Removed after facebook rejection
        const handleFacebookLogin = () => {
            Plugins.OAuth2Client.authenticate({
                appId: FACEBOOK_ID,
                scope: "email",
                authorizationBaseUrl: "https://www.facebook.com/v5.0/dialog/oauth",
                accessTokenEndpoint:  "https://graph.facebook.com/v5.0/oauth/access_token",
                resourceUrl: "https://graph.facebook.com/v5.0/me?fields=id,email,name",
                web: {
                    redirectUrl: FACEBOOK_CALLBACK,
                    windowOptions: "height=400,left=0,top=0"
                },
                android: {
                    customHandlerClass: "com.example.convivo.YourAndroidFacebookOAuth2Handler",
                },
            }).then(resourceUrlResponse => {
                const accessToken = resourceUrlResponse["access_token"];
                const oauthUserId = resourceUrlResponse["id"];
                const email = resourceUrlResponse["email"];
                const name = resourceUrlResponse["name"];
                handleReturnData(oauthUserId, email, name, accessToken);
            }).catch(reason => {
                console.error("OAuth rejected", reason);
                setError(true);
            });
        }
    */

    const handleFallbackLogin = async () => {
        const login = await fetch(API_AUTH_ENDPOINT, {
            headers: form
        });
        if(login.status == 200) {
            const payload = await login.json();
            handleReturnData(payload['userId'], form['Username'], '', payload['accessToken']);
        } else {
            setError('wrongunpswd');
        }
    }

    const handleFallbackSignup = async () => {
        const signup = await fetch(API_AUTH_ENDPOINT, {
            method: 'POST',
            headers: form
        });
        if(signup.status == 200) {
            const payload = await signup.json();
            handleReturnData(payload['userId'], form['Username'], '', payload['accessToken']);
        } else {
            setError('alreadyexists');
        } 
    }

    const handleReturnData = async (id, email, name, accessToken) => {
        const user = await retrieveUser(id, accessToken);
        const data = {
            userId: id,
            email: email,
            name: name,
            notifications: user ? user.notifications : [],
            profile: user && user.profile,
            accessToken: accessToken,
        };
        console.log(data);
        passData(data);
    }

    const retrieveUser = async (userID, accessToken) => {
        const req = await fetch(API_USERS_ENDPOINT + '/' + userID, {
            headers: {
                'Authorization': 'Bearer ' + accessToken
            }
        });
        if(req.status == 200) {
            //User has a profile already
            const payload = await req.json();
            return payload;
        } else if(req.status == 404) {
            //User is registering for the first time
            return null;
        } else if(req.status == 403) {
            //Should not happen, we just got token
            console.log('whattt');
            return null;
        }
    }

    return (
        <div style={{marginTop: 50}}>
            <TextField
                label="Email address"
                type='email'
                value={form.Username}
                onChange={(e) => {const nv = e.target.value; setForm(form => ({...form, Username:nv}))}}                           
                placeholder="Email address"
                
            /><br/>
            <TextField
                label="Password"
                type='password'
                value={form.Password}
                onChange={(e) => {const nv = e.target.value; setForm(form => ({...form, Password:nv}))}}                      
                placeholder="Password"
                
            /><br/>
            <Button className={classes.loginButton} onClick={handleFallbackSignup}>Sign up</Button>
            <Button className={classes.loginButton} onClick={handleFallbackLogin}>Login</Button>
            {error && <Box className={classes.errorBox}>
                <ErrorIcon color='error'/>
                <Typography color='error' className={classes.alert}>
                    &nbsp;{error == 'alreadyexists'
                    ? "That username is already taken."
                    : "Wrong username or password."}
                </Typography>
            </Box>}
        </div>
    )
}

/** Removed after Facebook rejection
<Button className={classes.loginbutton + ' ' + classes.facebook} onClick={handleFacebookLogin}>Login with facebook</Button>
{error && <Box className={classes.errorBox}>
                <ErrorIcon color='error'/>
                <Typography color='error' className={classes.alert}>
                    &nbsp;Something went wrong, please try again in a minute.
                </Typography>
            </Box>}

            //error must be reverted to bool
*/