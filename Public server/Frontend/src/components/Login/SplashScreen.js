import React from 'react'
import { Typography } from '@material-ui/core';
import Login from './Login';

/** Wrapper for the login component */

export default function SplashScreen(props) {
    const {passData} = props;
    return (
        <div>
            <Typography variant='h2' gutterBottom>Welcome to Convivo!</Typography>
            <Typography variant='body1'>Log in or sign up using your email and password.</Typography>
            <Login passData={passData}/>
        </div>
    )
}
