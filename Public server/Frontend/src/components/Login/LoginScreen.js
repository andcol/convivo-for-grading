import React from 'react'
import { Typography } from '@material-ui/core';
import Login from './Login';

/** Wrapper for the login component */

export default function ExpiredScreen(props) {
    const {passData} = props;
    return (
        <div>
            <Typography variant='h2' gutterBottom>Log in</Typography>
            <Typography variant='body1'>Seems like you need to log in again.</Typography>
            <Login passData={passData}/>
        </div>
    )
}
