import React, {useState} from 'react';

import {useHistory} from 'react-router-dom';
import { IconButton, Menu, MenuItem, Avatar, Typography } from '@material-ui/core';

/** A simple component which handles operations on the current account (e.g. logout) */

export default function UserTools(props) {

    const {avatarUrl, authenticated, handleLogout} = props;

    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    const history = useHistory();

    const handleProfile = () => {
        history.push('/profile');
    }

    const handleMenu = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div>
            {authenticated && (
                <div>
                <IconButton
                    aria-label="account of current user"
                    aria-controls="menu-appbar"
                    aria-haspopup="true"
                    onClick={handleMenu}
                    color="inherit"
                >
                    <Avatar src={avatarUrl}/>
                </IconButton>
                <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                    }}
                    keepMounted
                    transformOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                    }}
                    open={open}
                    onClose={handleClose}
                >
                    <MenuItem onClick={handleProfile}>Edit profile</MenuItem>
                    <MenuItem onClick={handleLogout}>Logout</MenuItem>
                </Menu>
                </div>
            )}
        </div>
    )
}
