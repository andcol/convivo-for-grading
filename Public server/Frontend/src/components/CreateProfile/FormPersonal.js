import React, { useState } from 'react'
import ErrorIcon from '@material-ui/icons/Error'
import deburr from 'lodash.deburr'
import { Box, TextField, FormControl, InputLabel, Select, MenuItem, FormHelperText, Button, Container, Typography, Paper, MenuList, Popper } from '@material-ui/core';
import Downshift from 'downshift';

const developmentPrefix = 'http://cors-anywhere.herokuapp.com/';
const cityACEndpoint = 'http://gd.geobytes.com/AutoCompleteCity?&q=';
const countryACEndpoint = 'http://autocomplete.travelpayouts.com/places2?locale=en&types[]=country&term=';


export default function FormPersonal(props) {
    const [targetCitySuggestions, setTargetCitySuggestions] = useState([]);
    const [nationalitySuggestions, setNationalitySuggestions] = useState([]);
    const [error, setError] = useState(false);

    const { values, handleChange, handleNationalityChange, handleTargetCityChange, abort, nextStep } = props;
    const { classes } = props;

    //The following functions are adapted from https://material-ui.com/components/autocomplete/ to handle autocompletion

    function renderInput(inputProps) {
        const { InputProps, classes, ref, ...other } = inputProps;
        
        return (
            <TextField
            InputProps={{
                inputRef: ref,
                classes: {
                root: classes.inputRoot,
                input: classes.inputInput,
                },
                ...InputProps,
            }}
            {...other}
            />
        );
    }

    function renderSuggestion(suggestionProps) {
        const { suggestion, index, itemProps, highlightedIndex, selectedItem } = suggestionProps;
        const isHighlighted = highlightedIndex === index;
        const isSelected = (selectedItem || '').indexOf(suggestion.label) > -1;
        
        return (
            <MenuItem
            {...itemProps}
            key={suggestion.label}
            selected={isHighlighted}
            component="div"
            style={{
                fontWeight: isSelected ? 500 : 400,
            }}
            >
            {suggestion.label}
            </MenuItem>
        );
    }

    function getNationalitySuggestions(value, { showEmpty = false } = {}) {
        const inputValue = deburr(value.trim()).toLowerCase();
        const inputLength = inputValue.length;
        let count = 0;
        
        return inputLength === 0 && !showEmpty
            ? []
            : nationalitySuggestions.filter(suggestion => {
                const keep =
                count < 5 && suggestion.label.slice(0, inputLength).toLowerCase() === inputValue;
        
                if (keep) {
                count += 1;
                }
        
                return keep;
            });
    }

    function getTargetCitySuggestions(value, { showEmpty = false } = {}) {
        const inputValue = deburr(value.trim()).toLowerCase();
        const inputLength = inputValue.length;
        let count = 0;
        
        return inputLength === 0 && !showEmpty
            ? []
            : targetCitySuggestions.filter(suggestion => {
                const keep =
                count < 5 && suggestion.label.slice(0, inputLength).toLowerCase() === inputValue;
        
                if (keep) {
                count += 1;
                }
        
                return keep;
            });
    }

    //End of the imported functions

    const handleNext = () => {
        if (values.displayName !== ''
                && values.age !== ''
                && values.gender !== ''
                && values.nationality !== ''
                && values.targetCity !== ''
                && values.personality !== ''
        ) {
            setError(false);
            nextStep();
        } else {
            console.log(values.targetCity);
            setError(true);
        }
    }

    const handleNationality = async e => {
        const query = e.target.value;
        const response = await fetch(countryACEndpoint + query);
        if (response.ok) {
            var list = await response.json();
            list = list.map(suggestion => ({label:suggestion['name'] + ' (' + suggestion['code'] + ')'}));
            setNationalitySuggestions(list);
        } else {
            setNationalitySuggestions([]);
        }
           
    }

    const handleTargetCity = async e => {
        const query = e.target.value;
        const response = await fetch(developmentPrefix + cityACEndpoint + query);
        if (response.ok) {
            var list = await response.json();
            if (list[0] === '%s' || list[0] === "") {
                //input is too short or has no match
                list = [];
            } else {
                list = list.map(suggestion => ({label:suggestion}));
            }            
            setTargetCitySuggestions(list);  
        } else {
            setTargetCitySuggestions([]);
        }
        
    }
    
    console.log(values);

    return (
        <React.Fragment>
        <Container>
        <Typography gutterBottom variant='h3' className={classes.sectionTitle}>Create a profile</Typography>
        <Box className={classes.rootBox}>
            <Typography gutterBottom variant='body1'>
                Thank you for joining Convivo!
                To get started, we need you to create a profile with your personal information and your preference.
                First of all, we'd like you to tell us about yourself.
            </Typography>
            <br/>
            <TextField
                label="Name"
                value={values.displayName}
                onChange={handleChange(null, "displayName")}                       
                placeholder="What do you want others to call you?"
                
            /><br/>
            <TextField
                label="Age"
                type='number'
                value={values.age}
                onChange={handleChange("personalData", "age")}                       
                placeholder="How old are you?"
                
            /><br/>
            <FormControl >
                <InputLabel>Gender</InputLabel>
                <Select
                    value={values.gender}
                    onChange={handleChange("personalData", "gender")}
                    name="gender"
                    className={classes.alignedSelect}
                    >
                    <MenuItem value="m">Male</MenuItem>
                    <MenuItem value="f">Female</MenuItem>
                    <MenuItem value="n">Non-binary</MenuItem>
                    <MenuItem value="u">Undisclosed</MenuItem>
                </Select>
                <FormHelperText>What gender do you identify as?</FormHelperText>
            </FormControl><br/>

            <Downshift id="downshift-nationality">
                {({
                getInputProps,
                getItemProps,
                getMenuProps,
                getLabelProps,
                highlightedIndex,
                inputValue,
                isOpen,
                selectedItem,
                }) => {
                const { onBlur, onFocus, ...inputProps } = getInputProps({
                    placeholder: 'Where do you come from?',
                    onChange: handleNationality,
                });

                return (
                    <div className={classes.container}>
                    {renderInput({
                        fullWidth: true,
                        classes,
                        InputLabelProps: getLabelProps({ shrink: values.nationality !== '' || onFocus }),
                        label: 'Nationality',
                        InputProps: { onBlur, onFocus },
                        inputProps,
                    })}

                    <div {...getMenuProps()}>
                        {isOpen ? (
                        <Paper className={classes.paper} square>
                            {getNationalitySuggestions(inputValue).map((suggestion, index) =>
                            renderSuggestion({
                                suggestion,
                                index,
                                itemProps: getItemProps({
                                    item: suggestion.label,
                                    onClick: handleNationalityChange
                                }),
                                highlightedIndex,
                                selectedItem,
                            }),
                            )}
                        </Paper>
                        ) : null}
                    </div>
                    </div>
                );
                }}
            </Downshift><br/>

            <Downshift id="downshift-city">
                {({
                getInputProps,
                getItemProps,
                getMenuProps,
                getLabelProps,
                highlightedIndex,
                inputValue,
                isOpen,
                selectedItem,
                }) => {
                const { onBlur, onFocus, ...inputProps } = getInputProps({
                    placeholder: 'What city are you looking to live in?',
                    onChange: handleTargetCity,
                });

                return (
                    <div className={classes.container}>
                    {renderInput({
                        fullWidth: true,
                        classes,
                        InputLabelProps: getLabelProps({ shrink: values.targetCity !== '' || onFocus }),
                        label: 'City',
                        InputProps: { onBlur, onFocus },
                        inputProps,
                    })}

                    <div {...getMenuProps()}>
                        {isOpen ? (
                        <Paper className={classes.paper} square>
                            {getTargetCitySuggestions(inputValue).map((suggestion, index) =>
                            renderSuggestion({
                                suggestion,
                                index,
                                itemProps: getItemProps({
                                    item: suggestion.label,
                                    onClick: handleTargetCityChange
                                }),
                                highlightedIndex,
                                selectedItem,
                            }),
                            )}
                        </Paper>
                        ) : null}
                    </div>
                    </div>
                );
                }}
            </Downshift><br/>

            <FormControl >
                <InputLabel>Personality</InputLabel>
                <Select
                    value={values.personality}
                    onChange={handleChange("personalData", "personality")}
                    name="personality"
                    className={classes.alignedSelect}
                    >
                    <MenuItem value="i">Introvert</MenuItem>
                    <MenuItem value="e">Extrovert</MenuItem>
                </Select>
                <FormHelperText>How would you describe yourself?</FormHelperText>
            </FormControl><br/>
            <Box className={classes.buttonBox}>
                <Button color="secondary" onClick={abort}>Cancel</Button>
                <Button color="primary" onClick={handleNext}>Next</Button>
            </Box>
            {error && <Box className={classes.errorBox}>
                <ErrorIcon color='error'/>
                <Typography align='right' color='error' className={classes.alert}>
                    &nbsp;all fields are mandatory.
                </Typography>
            </Box>}
        </Box>
        </Container>
        </React.Fragment>
    )
}