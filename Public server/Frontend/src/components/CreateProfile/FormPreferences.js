import React, { useState } from 'react'
import ErrorIcon from '@material-ui/icons/Error';
import AttachMoney from '@material-ui/icons/AttachMoney'
import { Box, TextField, FormControl, InputLabel, Select, MenuItem, FormHelperText, Button, Container, Typography, InputAdornment } from '@material-ui/core';

//The same scale is used to represent different data
const preferenceChoices = [
    <MenuItem value={2}>That's a must</MenuItem>,
    <MenuItem value={1}>I'd like that</MenuItem>,
    <MenuItem value={0}>I don't care</MenuItem>,
    <MenuItem value={-1}>I wouldn't like that</MenuItem>,
    <MenuItem value={-2}>That's a deal-breaker</MenuItem>
];
const importanceChoices = [
    <MenuItem value={2}>It's absolutely necessary</MenuItem>,
    <MenuItem value={1}>It's important</MenuItem>,
    <MenuItem value={0}>I don't care</MenuItem>
];

export default function FormPreferences(props) {
    const [error, setError] = useState(false);

    const { values, handleChange, prevStep, nextStep } = props;
    const { classes } = props;

    const handleNext = () => {
        if (values.sameAgePref !== ''
                && values.sameGenderPref !== ''
                && values.sameNationalityPref !== ''
                && values.livingPref !== ''
                && values.sameOccupationPref !== ''
                && values.guestsPref !== ''
                && values.childrenPref !== ''
                && values.petsPref !== ''
                && values.cleaningPref !== ''
                && values.drinkingPref !== ''
                && values.smokingPref !== ''
                && values.drugsPref !== ''
                && values.maxBudget !== ''
        ) {
            setError(false);
            nextStep();
        } else {
            setError(true);
        }
    } 

    console.log(values);

    
    return (
        <Container>
        <Typography gutterBottom variant='h3' className={classes.sectionTitle}>Personal preferences</Typography>
        <Box className={classes.rootBox}>
            <Typography gutterBottom variant='body1'>
                Lastly, we'd like to know what you're looking for in a potential roommate.
            </Typography>
            <br/>
                <FormControl className={classes.largeInputField}>
                    <InputLabel>Same age</InputLabel>
                    <Select
                        value={values.sameAgePref}
                        onChange={handleChange('preferences',"sameAgePref")}
                        name="sameAgePref"
                        className={classes.alignedSelect}>
                            {importanceChoices}
                    </Select>
                    <FormHelperText>How important is it for you to live with people your age?</FormHelperText>
                </FormControl><br/>
                <FormControl className={classes.largeInputField}>
                    <InputLabel>Same gender</InputLabel>
                    <Select
                        value={values.sameGenderPref}
                        onChange={handleChange('preferences',"sameGenderPref")}
                        name="sameGenderPref"
                        className={classes.alignedSelect}>
                            {importanceChoices}
                    </Select>
                    <FormHelperText>How important is it for you to live with people of the same gender?</FormHelperText>
                </FormControl><br/>
                <FormControl className={classes.largeInputField}>
                    <InputLabel>Same nationality</InputLabel>
                    <Select
                        value={values.sameNationalityPref}
                        onChange={handleChange('preferences',"sameNationalityPref")}
                        name="sameNationalityPref"
                        className={classes.alignedSelect}>
                            {importanceChoices}
                    </Select>
                    <FormHelperText>How important is it for you to live with people from the same country?</FormHelperText>
                </FormControl><br/>

                <FormControl className={classes.largeInputField}>
                    <InputLabel>Communal living</InputLabel>
                    <Select
                        value={values.livingPref}
                        onChange={handleChange('preferences',"livingPref")}
                        name="livingPref"
                        className={classes.alignedSelect}>
                            {preferenceChoices}
                    </Select>
                    <FormHelperText>How do you feel about sharing stuff and communal living?</FormHelperText>
                </FormControl><br/>

                <FormControl className={classes.largeInputField}>
                    <InputLabel>Same occupation</InputLabel>
                    <Select
                        value={values.sameOccupationPref}
                        onChange={handleChange('preferences',"sameOccupationPref")}
                        name="sameOccupationPref"
                        className={classes.alignedSelect}>
                            {preferenceChoices}
                    </Select>
                    <FormHelperText>How do you feel about roommates with your same occupation?</FormHelperText>
                </FormControl><br/>
                <FormControl className={classes.largeInputField}>
                    <InputLabel>Guests</InputLabel>
                    <Select
                        value={values.guestsPref}
                        onChange={handleChange('preferences',"guestsPref")}
                        name="guestsPref"
                        className={classes.alignedSelect}>
                            {preferenceChoices}
                    </Select>
                    <FormHelperText>How do you feel about your roommates having guests?</FormHelperText>
                </FormControl><br/>

                <FormControl className={classes.largeInputField}>
                    <InputLabel>Children</InputLabel>
                    <Select
                        value={values.childrenPref}
                        onChange={handleChange('preferences',"childrenPref")}
                        name="childrenPref"
                        className={classes.alignedSelect}>
                            {preferenceChoices}
                    </Select>
                    <FormHelperText>How do you feel about having roommates with children?</FormHelperText>
                </FormControl><br/>

                <FormControl className={classes.largeInputField}>
                    <InputLabel>Pets</InputLabel>
                    <Select
                        value={values.petsPref}
                        onChange={handleChange('preferences',"petsPref")}
                        name="petsPref"
                        className={classes.alignedSelect}>
                            {preferenceChoices}
                    </Select>
                    <FormHelperText>How do you feel about having roommates with pets?</FormHelperText>
                </FormControl><br/>

                <FormControl className={classes.largeInputField}>
                    <InputLabel>Cleaning</InputLabel>
                    <Select
                        value={values.cleaningPref}
                        onChange={handleChange('preferences',"cleaningPref")}
                        name="cleaningHabits"
                        className={classes.alignedSelect}>
                            {importanceChoices}
                    </Select>
                    <FormHelperText>How important is it for you to live with clean people?</FormHelperText>
                </FormControl><br/>

                <FormControl className={classes.largeInputField}>
                    <InputLabel>Drinking</InputLabel>
                    <Select
                        value={values.drinkingPref}
                        onChange={handleChange('preferences',"drinkingPref")}
                        name="drinkingPref"
                        className={classes.alignedSelect}>
                            {preferenceChoices}
                    </Select>
                    <FormHelperText>How do you feel about living with drinkers?</FormHelperText>
                </FormControl><br/>

                <FormControl className={classes.largeInputField}>
                    <InputLabel>Smoking</InputLabel>
                    <Select
                        value={values.smokingPref}
                        onChange={handleChange('preferences',"smokingPref")}
                        name="smokingPref"
                        className={classes.alignedSelect}>
                            {preferenceChoices}
                    </Select>
                    <FormHelperText>How do you feel about living with smokers?</FormHelperText>
                </FormControl><br/>

                <FormControl className={classes.largeInputField}>
                    <InputLabel>Substances</InputLabel>
                    <Select
                        value={values.drugsPref}
                        onChange={handleChange('preferences',"drugsPref")}
                        name="drugsPref"
                        className={classes.alignedSelect}>
                            {preferenceChoices}
                    </Select>
                    <FormHelperText>How do you feel about living with substance users?</FormHelperText>
                </FormControl><br/>
                <TextField
                label="Budget"
                type='number'
                value={values.maxBudget}
                onChange={handleChange('preferences',"maxBudget")}
                InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        $
                      </InputAdornment>
                    ),
                  }}                       
                placeholder="How much are you willing to contribute to rent and bills?"
                className ={classes.largeInputField}
            /><br/>
                <Box className={classes.buttonBox}>
                    <Button color="secondary" onClick={prevStep}>Back</Button>
                    <Button color="primary" onClick={handleNext}>Next</Button>
                </Box>
                {error && <Box className={classes.errorBox}>
                    <ErrorIcon color='error'/>
                    <Typography align='right' color='error' className={classes.alert}>
                        &nbsp;all fields are mandatory.
                    </Typography>
                </Box>}
            </Box>
            </Container>
    )
}

