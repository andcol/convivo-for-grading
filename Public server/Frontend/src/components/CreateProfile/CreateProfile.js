import React, {useState, useEffect} from 'react'
import FormPersonal from './FormPersonal';
import FormLiving from './FormLiving';
import FormPreferences from './FormPreferences';
import { makeStyles } from '@material-ui/styles';
import {useHistory} from 'react-router-dom';
import FormExtras from './FormExtras';
import FormDoneMandatory from './FormDoneMandatory';
import { API_USERS_ENDPOINT } from '../../constants';



const useStyles = makeStyles(theme => ({
    rootBox: {
        display: 'flex',
        flexDirection: 'column',
        width: 500,
        margin: 'auto',
        marginBottom: 48,
        textAlign: 'left',
        [theme.breakpoints.down('sm')]: {
            width: '90%'
        },
        [theme.breakpoints.up('sm')]: {
            marginLeft: 40,
            justifyContent: 'flex-start'
        }
    },
    buttonBox: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    errorBox: {
        justifyContent: 'flex-end',
        display: 'flex',
        alignItems: 'center'
    },
    alignedSelect: {
        textAlign: 'left'
    },
    sectionTitle: {
        marginTop: 48,
        [theme.breakpoints.up('sm')]: {
            marginLeft: 32,
            textAlign: 'left'
        }
    }
}));

const newProfile = {
    displayName: '',
    targetCity: '',
    bio: '',
    socialMedia: {
        facebook: '',
        twitter: '',
        instagram: ''
    },
    personalData: {
        age: '',
        gender: '',
        nationality: {
            name: '',
            code: ''
        },
        personality: '',
    },
    livingData: {
        occupation: '',
        hasPets: '',
        hasChildren: '',
        cleaningHabits: '',
        drinkingHabits: '',
        smokingHabits: '',
        drugsHabits: '',
        guestsHabits: '',
    },
    preferences: {
        maxBudget: '',
        sameGenderPref: '',
        sameNationalityPref: '',
        sameAgePref: '',
        sameOccupationPref: '',
        livingPref: '',
        childrenPref: '',
        petsPref: '',
        cleaningPref: '',
        smokingPref: '',
        drinkingPref: '',
        drugsPref: '',
        guestsPref: '',
    },
    extraData: {
        personalTags: [],
        interestsTags: [],
    },
};

/** This and the other components in the same folder make up the profile creation form */

function CreateProfile(props) {

    const {userData, setUserProfile, handleLogout} = props;

    const [step, setStep] = useState(1);
    const [values, setValues] = useState({...newProfile,displayName:userData.name});
    
    const history = useHistory();

    const classes = useStyles();

    const nextStep = () => {
        window.scrollTo(0,0);
        setStep(step => step + 1);
    }

    const prevStep = () => {
        window.scrollTo(0,0);
        setStep(step => step - 1);
    }

    const handleChange = (section, field) => e => {
        let newValue = e.target.value;
        if (field === 'age' || field === 'maxBudget') {
            //convert numeric fields
            newValue = Number(newValue);
        }
        if(section) {
            setValues(state => ({...state, [section]: {...state[section], [field]: newValue }}));
        } else {
            setValues(state => ({...state, [field]: newValue}));
        }
    }

    const handleNationalityChange = e => {
        //split nationality name and code
        const strings = e.target.textContent.split('(');
        const newValue = {name: strings[0].slice(0,-1), code: strings[1].toLowerCase().slice(0,-1)};
        setValues(state => ({...state, personalData: {...state.personalData, nationality: newValue}}));            
    }

    const handleTargetCityChange = e => {
        let newValue = e.target.textContent;
        setValues(state => ({...state, targetCity: newValue}));
    }

    const handleAddTag = type => tag => {
        const newTags = values.extraData[type];
        newTags.push(tag);
        setValues(state => ({...state, extraData:{...state.extraData, [type]:newTags}}));
    }

    const handleDeleteTag = type => (tag, index) => {
        const newTags = values[type];
        newTags.splice(index, 1);
        setValues(state => ({...state, extraData:{...state.extraData, [type]:newTags}}));
    }

    const abort = () => {
        handleLogout();
    }

    const done = async () => {
        //PUT user in the database
        const payload = {...values, email:userData.email};
        const p = await fetch(API_USERS_ENDPOINT + '/' + userData.userId, {
            method: 'PUT',
            headers: {
                'Authorization': 'Bearer ' + userData.accessToken,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload),
        });
        if(p.status == 200) {
            //update local profile state
            setUserProfile(values);
        }
    }

    switch (step) {
        case 1: return (
            <FormPersonal
                abort = {abort}
                nextStep = {nextStep}
                handleChange = {handleChange}
                handleNationalityChange = {handleNationalityChange}
                handleTargetCityChange = {handleTargetCityChange}
                values = {{...(({displayName, targetCity})=>({displayName, targetCity}))(values),...values.personalData}}
                classes = {classes}
            />
        );
        case 2: return (
            <FormLiving
                nextStep = {nextStep}
                prevStep = {prevStep}
                handleChange = {handleChange}
                values = {{...values.livingData}}
                classes = {classes}
            />
        );
            case 3:
            return(
            <FormPreferences
                nextStep = {nextStep}
                prevStep = {prevStep}
                handleChange = {handleChange}
                values = {{...values.preferences}}
                classes = {classes}
            />
            );
            case 4: return (
                <FormDoneMandatory
                prevStep = {prevStep}
                nextStep = {nextStep}
                done = {done}
                classes = {classes}
                />
            )
            case 5: return (
                <FormExtras
                prevStep ={prevStep}
                done = {done}
                handleChange = {handleChange}
                handleAddTag = {handleAddTag}
                handleDeleteTag = {handleDeleteTag}
                values = {{bio:values.bio,...values.extraData,...values.socialMedia}}
                classes = {classes}
                />
            );
            default: return <h1>Uh oh... something went wrong!</h1>;
        }
}

export default CreateProfile

