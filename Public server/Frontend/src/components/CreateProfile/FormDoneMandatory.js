import React from 'react'
import { Box, Container, Button, Chip } from '@material-ui/core'

export default function FormDoneMandatory(props) {
    const { prevStep, nextStep, done } = props;
    const { classes } = props;
    return (
        <Container>
        <h1 className={classes.sectionTitle}>Thank you!</h1>
        <Box className={classes.rootBox}>
            <p>
                You have succesfully entered all the information necessary to start using Convivo.
                If you wish to review or change any of your data, click on the <Chip label='review' size='small' color='secondary'/> button. 
                If you are happy with the data you entered and want to start looking for a roommate right now,
                click on <Chip label='done' size='small' color='primary'/>.
            </p>
            <p>
                If you have a minute more to spend, however, we would like to get to know you better.
                Click on <Chip label='next' size='small' color='primary'/> to enter additional information that can help us pair you with your perfect roommate!
            </p>
            <Box className={classes.buttonBox}>
                <Button color="secondary" onClick={prevStep}>Review</Button>
                <Button color="primary" onClick={nextStep}>Next</Button>
                <Button color="primary" onClick={done}>Done</Button>
            </Box>
        </Box>
        </Container>
    )
}
