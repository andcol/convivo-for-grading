import React, { useState } from 'react'
import ErrorIcon from '@material-ui/icons/Error';
import { Box, FormControl, InputLabel, Select, MenuItem, FormHelperText, Button, Container, Typography } from '@material-ui/core';

export default function FormLiving(props) {
    const [error, setError] = useState(false);

    const { values, handleChange, prevStep, nextStep } = props
    const { classes } = props

    const handleNext = () => {
        if (values.occupation !== ''
                && values.guestsHabits !== ''
                && values.hasChildren !== ''
                && values.hasPets !== ''
                && values.cleaningHabits !== ''
                && values.drinkingHabits !== ''
                && values.smokingHabits !== ''
                && values.drugsHabits !== ''
        ) {
            setError(false);
            nextStep();
        } else {
            setError(true);
        }
    }

    console.log(values);

    return (
        <Container>
        <Typography gutterBottom variant='h3' className={classes.sectionTitle}>What are your habits?</Typography>
        <Box className={classes.rootBox}>
            <Typography gutterBottom variant='body1'>
                Next, we'd like to know the way your are currently used to living.
                This will help us match you with people whose lifestyles do not clash with yours.
            </Typography>
            <br/>
                <FormControl className={classes.largeInputField}>
                    <InputLabel>Occupation</InputLabel>
                    <Select
                        value={values.occupation}
                        onChange={handleChange('livingData', "occupation")}
                        name="occupation"
                        className={classes.alignedSelect}
                        >
                        <MenuItem value="student">Study</MenuItem>
                        <MenuItem value="worker">Work</MenuItem>
                        <MenuItem value="nightWorker">Work (night shift)</MenuItem>
                    </Select>
                    <FormHelperText>What do you do for a living</FormHelperText>
                </FormControl><br/>
                <FormControl className={classes.largeInputField}>
                    <InputLabel>Guests</InputLabel>
                    <Select
                        value={values.guestsHabits}
                        onChange={handleChange('livingData', "guestsHabits")}
                        name="guestsHabits"
                        className={classes.alignedSelect}
                        >
                        <MenuItem value={-1}>Never</MenuItem>
                        <MenuItem value={0}>Rarely</MenuItem>
                        <MenuItem value={1}>Often</MenuItem>
                    </Select>
                    <FormHelperText>How often do you have guests?</FormHelperText>
                </FormControl><br/>

                <FormControl className={classes.largeInputField}>
                    <InputLabel>Children</InputLabel>
                    <Select
                        value={values.hasChildren}
                        onChange={handleChange('livingData', "hasChildren")}
                        name="hasChildren"
                        className={classes.alignedSelect}
                        >
                        <MenuItem value={false}>No</MenuItem>
                        <MenuItem value={true}>Yes</MenuItem>
                    </Select>
                    <FormHelperText>Are you moving with children?</FormHelperText>
                </FormControl><br/>

                <FormControl className={classes.largeInputField}>
                    <InputLabel>Pets</InputLabel>
                    <Select
                        value={values.hasPets}
                        onChange={handleChange('livingData', "hasPets")}
                        name="hasPets"
                        className={classes.alignedSelect}
                        >
                        <MenuItem value={false}>No</MenuItem>
                        <MenuItem value={true}>Yes</MenuItem>
                    </Select>
                    <FormHelperText>Are you moving with pets?</FormHelperText>
                </FormControl><br/>

                <FormControl className={classes.largeInputField}>
                    <InputLabel>Cleaning</InputLabel>
                    <Select
                        value={values.cleaningHabits}
                        onChange={handleChange('livingData', "cleaningHabits")}
                        name="cleaningHabits"
                        className={classes.alignedSelect}
                        >
                        <MenuItem value={-1}>I don't really care about it</MenuItem>
                        <MenuItem value={0}>I can stand a little mess</MenuItem>
                        <MenuItem value={1}>I need to live in an immaculate place</MenuItem>
                    </Select>
                    <FormHelperText>How strict are you about cleaning?</FormHelperText>
                </FormControl><br/>

                <FormControl className={classes.largeInputField}>
                    <InputLabel>Drinking</InputLabel>
                    <Select
                        value={values.drinkingHabits}
                        onChange={handleChange('livingData', "drinkingHabits")}
                        name="drinkingHabits"
                        className={classes.alignedSelect}
                        >
                        <MenuItem value={-1}>No</MenuItem>
                        <MenuItem value={0}>Rarely</MenuItem>
                        <MenuItem value={1}>Yes</MenuItem>
                    </Select>
                    <FormHelperText>Do you drink?</FormHelperText>
                </FormControl><br/>

                <FormControl className={classes.largeInputField}>
                    <InputLabel>Smoking</InputLabel>
                    <Select
                        value={values.smokingHabits}
                        onChange={handleChange('livingData', "smokingHabits")}
                        name="smokingHabits"
                        className={classes.alignedSelect}
                        >
                        <MenuItem value={-1}>No</MenuItem>
                        <MenuItem value={0}>Rarely</MenuItem>
                        <MenuItem value={1}>Yes</MenuItem>
                    </Select>
                    <FormHelperText>Do you smoke?</FormHelperText>
                </FormControl><br/>

                <FormControl className={classes.largeInputField}>
                    <InputLabel>Substances</InputLabel>
                    <Select
                        value={values.drugsHabits}
                        onChange={handleChange('livingData', "drugsHabits")}
                        name="drugsHabits"
                        className={classes.alignedSelect}
                        >
                        <MenuItem value={-1}>No</MenuItem>
                        <MenuItem value={0}>Rarely</MenuItem>
                        <MenuItem value={1}>Yes</MenuItem>
                    </Select>
                    <FormHelperText>Do you make use of any other substances?</FormHelperText>
                </FormControl><br/>

                <Box className={classes.buttonBox}>
                    <Button color="secondary" onClick={prevStep}>Back</Button>
                    <Button color="primary" onClick={handleNext}>Next</Button>
                </Box>
                {error && <Box className={classes.errorBox}>
                <ErrorIcon color='error'/>
                <Typography align='right' color='error' className={classes.alert}>
                    &nbsp;all fields are mandatory.
                </Typography>
            </Box>}
            </Box>
            </Container>
    )
}

