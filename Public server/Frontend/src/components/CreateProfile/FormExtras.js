import React from 'react'
import { Container, Button, Box, TextField, Typography } from '@material-ui/core'
import ChipInput from 'material-ui-chip-input';

export default function FormExtras(props) {
    const { values, handleChange, handleAddTag, handleDeleteTag, prevStep, done } = props;
    const { classes } = props;

    console.log(values);

    return (
        <Container>
            <Typography gutterBottom variant='h3' className={classes.sectionTitle}>Additional information</Typography>
            <Box className={classes.rootBox}>

            <Typography variant='h5'>Personal tags</Typography>
            <p>
                Are you a vegan, a creative soul, or perhaps an LGBT activist? Attach tags to your profile so that others can
                get a feeling of who you are.
            </p>
            <p>
                Tags helps us match you with people that are more similar to you.
            </p>
            <ChipInput
                label="Personal tags"
                value={values.personalTags}
                onAdd={handleAddTag("personalTags")}
                onDelete={handleDeleteTag("personalTags")}
                placeholder='Press enter to add a tag'
                variant='outlined'
                margin='normal'
            /><br/>

            <Typography variant='h5'>Hobbies and interests</Typography>
            <p>
                Do you have a passion for fashion? Are you always up to date about the latest game releases?
                Are you a seasoned hiker? Tell people what your hobbies and interests are, so you have something to
                talk about right away!
            </p>
            <ChipInput
                label="Interests and Hobbies"
                value={values.interestsTags}
                onAdd={handleAddTag("interestsTags")}
                onDelete={handleDeleteTag("interestsTags")}
                placeholder='Press enter to add a tag'
                variant='outlined'
                margin='normal'
            /><br/>

            <Typography variant='h5'>Profile bio</Typography>
            <p>
                Preference and tags may be useful to us, but ultimately nothing is as convincing as your own words.
                If you wish, introduce yourself to others in a short paragraph.
            </p>
            <TextField
                label="Profile bio"
                value={values.bio}
                onChange={handleChange(null,'bio')}
                placeholder='Tell us something about yourself!'
                variant="outlined"
                margin='normal'
            /><br/>

            <Typography variant='h5'>Social media</Typography>
            <p>
                If you wish, you can leave a handle to your favorite social media platform, so that other users can get to know you better.
            </p>
            <TextField
                label="Instagram"
                value={values.instagram}
                onChange={handleChange('socialMedia','instagram')}
                placeholder='Your IG handle is whatever comes after instagram.com/...'
                variant="outlined"
                margin='normal'
            />
            <TextField
                label="Twitter"
                value={values.twitter}
                onChange={handleChange('socialMedia','twitter')}
                placeholder='Your Twitter handle is whatever comes after twitter.com/...'
                variant="outlined"
                margin='normal'
            />
            <TextField
                label="Facebook"
                value={values.facebook}
                onChange={handleChange('socialMedia','facebook')}
                placeholder='Your FB handle is whatever comes after facebook.com/...'
                variant="outlined"
                margin='normal'
            /><br/>

                <Box className={classes.buttonBox}>
                    <Button color="secondary" onClick={prevStep}>Back</Button>
                    <Button color='primary' onClick={done}>Done</Button>
                </Box>
            </Box>
        </Container>
    )
}
