import React, { useEffect, useState } from 'react'
import { Snackbar } from '@material-ui/core'

/** Just displays a snackbar with messages for the user */

export default function Notification(props) {
    const {msg, trigger, setTrigger} = props;

    const handleClose = () => {
        setTrigger(false);
    }

    return (
        <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={trigger}
        autoHideDuration={5000}
        onClose={handleClose}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={<span id="message-id">{msg}</span>}
      />
    )
}
