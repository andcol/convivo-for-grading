export const makeProfilePhotoUrl = async (userId) => {
    return '/res/placeholder.png'
    /** Removed after Facebook rejection
        const res = await fetch(`https://graph.facebook.com/v5.0/${userId}/picture?height=300&redirect=0`);
        if(res.status == 200) {
            const payload = await res.json();
            if(payload.data.is_silhouette) {
                return '/res/placeholder.png'
            } else {
                return payload.data.url;
            }
        } else {
            return '/res/placeholder.png'
        }
    */
}